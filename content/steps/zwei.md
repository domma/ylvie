+++
title = "Start"
date = 2021-01-01
path = "zwei"
+++


> Eine ältere Frau möchte an diesem Sonntag einen sehr schönen und viel jüngeren
> Mann heiraten. Ihr bester Freund rät ihr davon ab. „Du bist doch dreimal so
> alt wie er!“ warf er ihr vor. Diese konterte aber
> gelassen:“Ja! Aber in 20 Jahren bin ich nur noch doppelt so alt wie er".


{{ question(text="Wie alt ist sie jetzt? (als Zahl)", answer="60", target="/60") }}

