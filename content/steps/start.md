+++
title = "Start"
date = 2021-01-01
path = "start"
+++

Um deinen ersten Hinweis zu bekommen, mußt du ein Rätsel lösen. Gib' die
Antwort in das Feld ein und drücke auf den grünen Knopf. Wird der Knopf rot,
war die Antwort falsch und du mußt nochmal überlegen.

> Peters Mutter hat 4 Kinder. Das erste Kind wurde auf den Namen „Januar“
> getauft. Das zweite Kind hat den Namen „März“ bekommen. Das dritte Kind hört
> auf den Namen „Mai“.


{{ question(text="Wie heißt das vierte Kind?", answer="peter", target="/peter") }}

