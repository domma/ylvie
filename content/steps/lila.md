+++
title = "Start"
date = 2021-01-01
path = "lila"
+++

Wunderbar! Du bist bereit für das nächste Rätsel!

> Fünf Freunde treffen sich an einem Montagabend in ihrer Stammkneipe und wollen
> ein kleines Dart-Turnier veranstalten. Jeder der Spieler muss bei diesem
> Turnier einmal gegen jeden spielen. Ein Spiel kostet am Dartautomaten 2.-Euro.

{{ question(text="Wie viele Geld müssen die Freunde insgesamt für die Spiele ausgeben? (als Zahl)", answer="20", target="/20") }}

