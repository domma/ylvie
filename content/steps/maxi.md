+++
title = "Start"
date = 2021-01-01
path = "maxi"
+++

Wunderbar! Du bist bereit für das nächste Rätsel!

> Welches Wort in einem Wörterbuch aus dem Jahr 2019 wird falsch geschrieben?

{{ question(text="Welches Wort ist es?", answer="falsch", target="/falsch") }}

