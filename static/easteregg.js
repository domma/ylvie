console.log("hello!");

const application = Stimulus.Application.start()

application.register("question", class extends Stimulus.Controller {
	connect() {
		console.log("connect");

		this.answer = this.element.getAttribute("data-answer").toLowerCase();
		this.target = this.element.getAttribute("data-target").toLowerCase();

		this.input = this.element.getElementsByClassName("answer")[0];
		this.button = this.element.getElementsByTagName("button")[0];
	}

	check() {
		let answer = this.input.value.toLowerCase();

		if (answer == this.answer) {
			console.log("ok");
			window.location = this.target;

		} else {
			console.log("bad");

			let button = this.button;

			this.input.value = "";
			button.classList.add("is-danger");
			button.classList.remove("is-success");

			setTimeout(
				function() {
					button.classList.remove("is-danger");
					button.classList.add("is-success");
				}, 1000);

		}


	}
});


console.log("ok");
